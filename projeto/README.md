# Projeto

* v0.0.1: Projeto Oficial com o [angular cli](https://cli.angular.io/)
*  v0.0.2: Add: dicas de branch no readme
* v0.0.3: Adicionando dependências
   - Add:  _( Editor Rico )_
https://killercodemonkey.github.io/ngx-quill-example/
   - Add: _( Arvore de Arquivos )_
https://angular2-tree.readme.io/docs
* v0.0.4: Add: material-icon
* v0.0.5: Add: tree-view (component,service,model) 





# Padrões


#### Como nomear as Branches
```sh
new/frabnotz
new/foo
new/bar
test/foo
test/frabnotz
ver/foo
```

#### Benefícios, buscar branches rapidamente
```sh
$ git branch --list "test/*"
test/foo
test/frabnotz

$ git branch --list "*/foo"
new/foo
test/foo
ver/foo

$ gitk --branches="*/foo"
```
---
