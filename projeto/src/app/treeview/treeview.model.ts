export interface TreeView {
    id: string;
    name: string;
    nivel: string;
    tipo: string;
    autor: string;
    releaseDate: string;
    fileSize: string;
    url: string;
}