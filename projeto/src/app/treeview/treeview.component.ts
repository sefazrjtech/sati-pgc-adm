import { Component, OnInit } from '@angular/core';
import { TreeviewService } from 'src/app/treeview/treeview.service';
import { TreeView } from 'src/app/treeview/treeview.model';
import { Output , EventEmitter } from '@angular/core';
import { TreeNode, TreeModel, TREE_ACTIONS, KEYS, IActionMapping, ITreeOptions, TreeComponent } from 'angular-tree-component';
import { Pasta } from 'src/app/breadcrumb/pasta.model';
import { BreadcrumbService } from 'src/app/breadcrumb/breadcrumb.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ViewChild } from '@angular/core';
import { LocalStorageService } from 'src/app/shared/local-storage.service';
import {STORE_TREEVIEW} from '../app.api';



@Component({
  selector: 'app-treeview',
  templateUrl: './treeview.component.html',
  styleUrls: ['./treeview.component.css']
})
export class TreeviewComponent implements OnInit {

  constructor(private treeViewService: TreeviewService , 
              private breadcrumbService : BreadcrumbService,
              private spinner: NgxSpinnerService,
              private localStorageService: LocalStorageService ) { 
    this.carregarTodasPastas();
  }

  ngOnInit() {
      /** spinner starts on init */
      this.spinner.show();
  }

  @Output() listTreview: EventEmitter<TreeView[]>  = new EventEmitter<TreeView[]>();
  bredCrumbs: Pasta[];
  pastas: TreeView[];
  nodes : TreeView[];
  @Output() collectionId: EventEmitter<string>  = new EventEmitter<string>();
  @ViewChild('tree') tree;

  

  options = {
    getChildren: this.getChildren.bind(this),
    actionMapping: {
      mouse: {
        dblClick: (tree, node, $event) => {
          TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event);
        },
        expanderClick:(tree, node, $event) => {
          let isCollapsed = node.isCollapsed;
              
          if(isCollapsed===  true){
            this.carregarPastasArquivosContent(node.data.id);
            this.carregarBredCrumbs(node.data.id);
            this.collectionId.emit(node.data.id);
          }
          TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event);
      

        },click: (tree, node, $event) => {
          console.log("fechou");
        }
      },
      keys: {
        [KEYS.ENTER]: (tree, node, $event) => {
          node.expandAll();
        }
      }
    },
  }

  getChildren(node: any) {
    return new Promise((resolve, reject) => {
      this.spinner.show();
      this.treeViewService.listarPastasPorId(node.data.id)
      .then(response => {
        this.nodes = response;
      });
      setTimeout(() => resolve(this.nodes), 2000);
      setTimeout(() => {this.spinner.hide(); }, 3000);
      //this.carregarPastasArquivosContent(node.data.id);
    });
  }


  carregarPastasArquivosContent(collectionId: string){
     this.treeViewService.listarPastasArquivosPorId(collectionId)
    .then(response => {
      this.listTreview.emit(response);
    });
  } 

  carregarBredCrumbs(collectionId: string){
    this.breadcrumbService.obterBreadCrumbPorCollectionId(collectionId)
    .then(response => {
      //this.bredCrumbs.emit(response);
    });
  }

  carregarTodasPastas(){
    this.treeViewService.listarPastas()
    .then(response => {
      this.pastas = response;
    });
  }

  downloadsArquivo(node){
    console.log(node)
  }

  onMoveNode($event){
  }


  onEvent ($event){
    console.log($event.eventName);
    
    if($event.eventName == 'initialized'){
      const collectionId = this.localStorageService.getStore(STORE_TREEVIEW);
      if(collectionId != null && collectionId != ''){
        this.breadcrumbService.obterBreadCrumbPorCollectionId(collectionId).then((response) => {
          this.bredCrumbs = response;
          setTimeout(() => 
          this.carregarTreeView(this.bredCrumbs)
           , 2000);
        });
        this.collectionId.emit(collectionId);
      }else{
        this.spinner.hide();
      }
      // set null para a chave que está no local storege
      this.localStorageService.store(STORE_TREEVIEW,null);
   }
   
    if($event.eventName == 'toggleExpanded'){
    }

  }


  carregarTreeView(bredCrumbs: Pasta[]){
    let some = null;
    this.bredCrumbs.map((item,index) => {
      console.log(index+" collectionId: "+item);
      console.log(index+" collectionId: "+item.id);
      
      if(some == null){
        console.log("carrega  nivel 1");
        some = this.tree.treeModel.getNodeById(item.id);
        console.log(some);
        some.expand()  
      }else{
        console.log("carrega  nivel 2");
        some = some.data;
        console.log(some);
        //some.expand() 
      }
    })
  }
    
    

    
}
