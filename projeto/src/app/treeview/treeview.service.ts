import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TreeView } from './treeView.model';
import {API,STORE_TREEVIEW} from '../app.api';


@Injectable()
export class TreeviewService {

  constructor(private http: HttpClient) { }

  listarPastas(): Promise<TreeView[]>{
    return this.http.get<TreeView[]>(`${API}/pastas`)
    .toPromise()
    .then(response => response)
    .catch(error => {
      console.log(error);
      return null;
    });
 }

 listarPastasArquivosPorId(collectionId : string): Promise<TreeView[]>{
   return this.http.get<TreeView[]>(`${API}/pastas/${collectionId}`)
   .toPromise()
   .then(response => response)
   .catch(error => {
     console.log(error);
     return null;
   });
}

listarPastasPorId(collectionId : string): Promise<TreeView[]>{
  console.log(collectionId)
  return this.http.get<TreeView[]>(`${API}/pastas/todas/${collectionId}`)
  .toPromise()
  .then(response => response)
  .catch(error => {
    console.log(error);
    return null;
  });
}


}
