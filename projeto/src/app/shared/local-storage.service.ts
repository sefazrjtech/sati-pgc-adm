import { Inject, Injectable } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

// key that is used to access the data in local storage
const STORAGE_KEY = 'local_treeview';


@Injectable()
export class LocalStorageService {

  constructor(@Inject(SESSION_STORAGE) private storage:StorageService) { }

  public store(key: string ,object: any){
   this.storage.set(key,object)
  }

  public getStore(key: string): string{
    return this.storage.get(key);
  }

  public getStoreObject(key: string): any{
    return this.storage.get(key);
  }

}
