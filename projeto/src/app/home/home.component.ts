import { Component, OnInit } from '@angular/core';
import { TreeView } from 'src/app/treeview/treeView.model';
import { Pasta } from 'src/app/breadcrumb/pasta.model';
import { Input } from '@angular/core';
import { FileContent } from 'src/app/file/file-content.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  teste: string;
  pastas: TreeView[];
  file: FileContent;
  collectionId: string;
  @Input() bredCrumbs: Pasta[];
  flag: boolean;
 
  ngOnInit() {
  }

  loadFile($event){
    this.file = $event;
    this.flag = true;
  }

  loadCollectionIdBreadcrumb($event){
    this.collectionId = $event;
    this.flag = false;
  }

  loadCollectionIdFileList($event){
    this.collectionId = $event;
  }

  loadCollectionIdTreeview($event){
    this.collectionId = $event;
    this.flag = false;
    this.pastas = $event;
  }


}
