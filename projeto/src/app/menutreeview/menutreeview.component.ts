import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-menutreeview',
  templateUrl: './menutreeview.component.html',
  styleUrls: ['./menutreeview.component.css']
})
export class MenutreeviewComponent implements OnInit {

  @Input() collectionId: string;

  constructor() { }

  ngOnInit() {
  }

}
