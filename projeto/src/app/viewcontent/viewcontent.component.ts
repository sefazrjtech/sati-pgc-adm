import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { FileContent } from 'src/app/file/file-content.model';
import { NgxSpinnerService } from 'ngx-spinner';
import {STORE_EDIT_WIKI} from 'src/app/app.api';
import { LocalStorageService } from 'src/app/shared/local-storage.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-viewcontent',
  templateUrl: './viewcontent.component.html',
  styleUrls: ['./viewcontent.component.css']
})
export class ViewcontentComponent implements OnInit {


  @Input() file : FileContent;

  constructor(private spinner: NgxSpinnerService,
              private localStorageService: LocalStorageService,
              private route: Router) { }

  ngOnInit() {
    console.log(this.file.extensao)
      /** spinner starts on init */
      this.spinner.show();
      setTimeout(() => {
          this.spinner.hide();
      }, 1000);
  }

  editar(){
    console.log(this.file.conteudo)
    this.localStorageService.store(STORE_EDIT_WIKI,this.file);
    this.route.navigate(['/home/wiki/edit']);
  }

}
