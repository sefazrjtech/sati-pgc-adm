import { Component, OnInit } from '@angular/core';
import { UploadEvent, UploadFile, FileSystemFileEntry, FileSystemDirectoryEntry, FileSystemEntryMetadata } from 'ngx-file-drop';
import { Router } from '@angular/router';
import { FileService } from 'src/app/file/file.service';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/local-storage.service';
import {STORE_TREEVIEW} from '../app.api';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  constructor(private fileService: FileService,
      private route: Router,
      private routeActive: ActivatedRoute ,
      private localStorageService: LocalStorageService,
      private spinner: NgxSpinnerService) {
        this.collectionId = this.routeActive.snapshot.paramMap.get('id');
       }

      ngOnInit() {
      }

  collectionId: string;
  public files: UploadFile[] = [];
 
  public dropped(event: UploadEvent) {

    for (const droppedFile of event.files) {
 
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
         
          // Here you can access the real file
          console.log(droppedFile.relativePath, file);
 
          /**
          // You could upload it like this:
          const formData = new FormData()
          formData.append('logo', file, relativePath)
 
          // Headers
          const headers = new HttpHeaders({
            'security-token': 'mytoken'
          })
 
          this.http.post('https://mybackend.com/api/upload/sanitize-and-save-logo', formData, { headers: headers, responseType: 'blob' })
          .subscribe(data => {
            // Sanitized logo returned from backend
          })
          **/

          let f = new UploadFile(droppedFile.relativePath,fileEntry); 
          this.files.push(f);
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;        
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  public fileOver(event){
    console.log("File over :"+event);
  }
 
  public fileLeave(event){
    console.log(event);
  }

public upload(){

  this.spinner.show();
  this.files.map((item,index)=>{
      const fileEntry = item.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          const formData = new FormData()
          formData.append('file', file, item.relativePath)

          this.fileService.upload(formData,this.collectionId)
          .then(response=> {
            console.log("criado "+index);
          })

          console.log(item.relativePath, file);
        });
      console.log("Contador "+index);
    })
  
    setTimeout(() =>{
      this.localStorageService.store(STORE_TREEVIEW,this.collectionId)
      this.route.navigate(['/home'])
    },2000);
}

cancelar(){
  this.localStorageService.store(STORE_TREEVIEW,this.collectionId)
  this.route.navigate(['/home'])
}


}
