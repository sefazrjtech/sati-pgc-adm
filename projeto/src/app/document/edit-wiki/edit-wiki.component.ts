import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/shared/local-storage.service';
import {STORE_EDIT_WIKI} from 'src/app/app.api';
import { FileContent } from 'src/app/file/file-content.model';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {STORE_TREEVIEW} from 'src/app/app.api';
import { NgForm } from '@angular/forms';
import { FileService } from 'src/app/file/file.service';

@Component({
  selector: 'app-edit-wiki',
  templateUrl: './edit-wiki.component.html',
  styleUrls: ['./edit-wiki.component.css']
})
export class EditWikiComponent implements OnInit {

  constructor(private localStorageService: LocalStorageService,
              private route: Router,
              private routeActive: ActivatedRoute,
              private fileService: FileService ) { }

  ngOnInit() {

   this.file =  this.localStorageService.getStoreObject(STORE_EDIT_WIKI);
   this.collectionId = this.file.collectionId;
   console.log(this.file);
  }

  valueEditor: string;
  titulo: string;
  fileEdit: any = {};
  collectionId: string;
  file : FileContent;

  onSubmit(form:NgForm){

    this.fileService.editarWiki(this.file).then(response=> {
      this.route.navigate(['/home']);
       // armazena o collectionId no local storege
       this.localStorageService.store(STORE_TREEVIEW,this.collectionId);
       this.localStorageService.store(STORE_EDIT_WIKI,null);
      });
  }

  
  cancelar(){

    this.localStorageService.store(STORE_TREEVIEW,this.collectionId)
    this.localStorageService.store(STORE_EDIT_WIKI,null);
    this.route.navigate(['/home'])
  }

}
