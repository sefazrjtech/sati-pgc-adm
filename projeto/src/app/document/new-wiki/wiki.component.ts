import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FileService } from 'src/app/file/file.service';
import { FileContent } from 'src/app/file/file-content.model';
import { ActivatedRoute } from '@angular/router';
import { Route } from '@angular/router';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/local-storage.service';
import {STORE_TREEVIEW} from 'src/app/app.api';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-wiki',
  templateUrl: './wiki.component.html',
  styleUrls: ['./wiki.component.css']
})
export class WikiComponent implements OnInit {

  constructor(private fileService: FileService,
              private route: Router,
              private routeActive: ActivatedRoute ,
              private localStorageService: LocalStorageService,
              private spinner: NgxSpinnerService ) { 
                this.collectionId = this.routeActive.snapshot.paramMap.get('id');
              }

  valueEditor: string;
  titulo: string;
  file: any = {};
  collectionId: string;

  ngOnInit() {}


  onSubmit(form:NgForm){
     if(form.valid){
      this.spinner.show();
       this.file.extensao = ".html";
       this.file.id = this.collectionId;
       this.fileService.criarWiki(this.file).then(response=> {
       this.route.navigate(['/home']);
        
        // armazena o collectionId no local storege
        this.localStorageService.store(STORE_TREEVIEW,this.collectionId);
       });
     }else{
        console.log("not valid")
     }
  }


  editWiki(){

  }

  cancelar(){
    this.localStorageService.store(STORE_TREEVIEW,this.collectionId)
    this.route.navigate(['/home'])
  }

}
