import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule , ActivatedRoute } from '@angular/router';
import { TreeModule } from 'angular-tree-component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CKEditorModule } from 'ngx-ckeditor';


import {ROUTES} from './app.routes';
import { TreeviewComponent } from './treeview/treeview.component'
import { TreeviewService } from 'src/app/treeview/treeview.service';
import { FilelistComponent } from './filelist/filelist.component';
import { ViewcontentComponent } from './viewcontent/viewcontent.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { BreadcrumbService } from 'src/app/breadcrumb/breadcrumb.service';

// img load//
import { NgxSpinnerModule } from 'ngx-spinner';
import { WikiComponent } from './document/new-wiki/wiki.component';
import { MenutreeviewComponent } from './menutreeview/menutreeview.component';
import { UploadComponent } from './upload/upload.component';
import { FileService } from 'src/app/file/file.service';
import { StorageServiceModule } from 'angular-webstorage-service'
import { LocalStorageService } from 'src/app/shared/local-storage.service';

import { FileDropModule } from 'ngx-file-drop';
import { EditWikiComponent } from './document/edit-wiki/edit-wiki.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    TreeviewComponent,
    FilelistComponent,
    ViewcontentComponent,
    BreadcrumbComponent,
    WikiComponent,
    MenutreeviewComponent,
    UploadComponent,
    EditWikiComponent  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    TreeModule.forRoot(),
    RouterModule.forRoot(ROUTES),
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    NgxSpinnerModule,
    CKEditorModule,
    StorageServiceModule ,
    FileDropModule
  ],
  providers: [TreeviewService , 
              BreadcrumbService,
              FileService,
              LocalStorageService],
              
  bootstrap: [AppComponent]
})
export class AppModule { }
