export class FileContent{
    id: string;
    name: string;
    url: string;
    extensao: string;
    arquivo: string[];
    autor: string;
    conteudo: string;
    size: string;
    dtCriacaoArquivo: string;
	dtUltimaModificacao: string;
	autorUltimaModificacao: string;
	revision: string;
    tipoDoc: string;
    collectionId: string;
}