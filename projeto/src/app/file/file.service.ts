import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {API} from '../app.api';
import { FileContent } from 'src/app/file/file-content.model';


@Injectable()
export class FileService {

  constructor(private http: HttpClient) { }

  criarWiki(file: any) : Promise<any> {
    return this.http.post("http://localhost:8080/arquivo/wiki",file)
    .toPromise()
    .then(response=> {
      console.log("Criado com sucesso"+response);
    }).catch(error=>{
      console.log("Error ao criar arquivo"+error);
      return null;
    });
  }


  editarWiki(file: any) : Promise<any> {
    return this.http.put("http://localhost:8080/arquivo/wiki",file)
    .toPromise()
    .then(response=> {
      console.log("editado com sucesso"+response);
    }).catch(error=>{
      console.log("Error ao editar arquivo"+error);
      return null;
    });
  }


   upload(formData :FormData,collectionId:string): Promise<String>{
    let headers = new Headers();
    formData.append('Content-Type', 'application/json');
    formData.append('Accept', `application/json`);
    formData.append("collectionId",collectionId);

    return this.http.post(`${API}/arquivo`,formData)
    .toPromise()
    .then(response => response)
    .catch(error => {
      console.log(error);
      return null;
    })
  }

  consultarFile(dDocName: string) : Promise<FileContent>{
    return this.http.get<FileContent>(`${API}/arquivo/${dDocName}`)
    .toPromise()
    .then(response => response )
    .catch(error => {
      console.log(error);
      return null;
    });
  }

}
