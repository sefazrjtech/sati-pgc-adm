import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { TreeView } from 'src/app/treeview/treeView.model';
import { TreeviewService } from 'src/app/treeview/treeview.service';
import {FileContent} from 'src/app/file/file-content.model';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BreadcrumbService } from 'src/app/breadcrumb/breadcrumb.service';
import { Pasta } from 'src/app/breadcrumb/pasta.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { FileService } from 'src/app/file/file.service';


@Component({
  selector: 'app-filelist',
  templateUrl: './filelist.component.html',
  styleUrls: ['./filelist.component.css']
})
export class FilelistComponent implements OnInit {

  constructor(private treeViewService: TreeviewService, 
              private fileService: FileService,
              private breadcrumbService: BreadcrumbService,
              private spinner: NgxSpinnerService) {

              }

  @Input() pastas: TreeView[];
  @Output() fileContent: EventEmitter<FileContent>  = new EventEmitter<FileContent>();
  fileResponse : FileContent;
  @Input() bredCrumbs: Pasta[];
  @Input()  collectionId: string;
  @Output() emitCollectionId: EventEmitter<string>  = new EventEmitter<string>(); 

  ngOnInit() {
  }

  ngOnChanges() {
    this.loadPastasTreeView();
  }

  clickPasta(id){
    this.collectionId = id;
    this.spinner.show()
    this.loadPastasTreeView();
    setTimeout(() => {this.spinner.hide();},1000);
  }

  clickArquivo(id){
    this.fileService.consultarFile(id)
    .then(response => {
      this.fileResponse = response
      this.fileResponse.collectionId = this.collectionId;
      this.fileContent.emit(this.fileResponse);
    });
  }

  retreiveHtml(id){
    this.fileService.consultarFile(id)
    .then(response => {
      this.fileResponse = response
      this.fileResponse.collectionId = this.collectionId;
      this.fileContent.emit(this.fileResponse);
    });
  }

  loadPastas($event){
    this.pastas = $event;
  }

  loadPastasTreeView(){
    this.treeViewService.listarPastasArquivosPorId(this.collectionId)
    .then(response => {
      this.pastas = response;
      this.emitCollectionId.emit(this.collectionId);
   })
  }

}
