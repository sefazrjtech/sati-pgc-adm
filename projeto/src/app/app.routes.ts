import {Routes} from '@angular/router';
import { HomeComponent } from 'src/app/home/home.component';
import { TreeviewComponent } from 'src/app/treeview/treeview.component';
import { WikiComponent } from 'src/app/document/new-wiki/wiki.component';
import { UploadComponent } from 'src/app/upload/upload.component';
import { EditWikiComponent } from 'src/app/document/edit-wiki/edit-wiki.component';

export const ROUTES: Routes = [
    {path: "home" , component: HomeComponent },
    {path: "home/wiki/new/:id" , component: WikiComponent },
    {path: "home/wiki/edit" ,component: EditWikiComponent},
    {path: "home/upload/:id" ,component: UploadComponent}
   
] 
   
