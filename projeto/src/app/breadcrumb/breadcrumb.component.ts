import { Component, OnInit } from '@angular/core';
import { Pasta } from 'src/app/breadcrumb/pasta.model';
import { Input } from '@angular/core';
import { TreeviewService } from 'src/app/treeview/treeview.service';
import { TreeView } from 'src/app/treeview/treeView.model';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BreadcrumbService } from 'src/app/breadcrumb/breadcrumb.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { FileContent } from 'src/app/file/file-content.model';

@Component({
  selector: 'breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {

  constructor(private treeViewService: TreeviewService ,
              private breadcrumbService : BreadcrumbService,
              private spinner: NgxSpinnerService,
              private route: ActivatedRoute) {
                  
                route.params.subscribe(params => {
                    if(params['id'] != null){
                      this.carregarBredCrumb(params['id']);
                    }
                  })
               }


  @Input() bredCrumbs: Pasta[];
  @Output() pastasEmmit : EventEmitter<TreeView[]>  = new EventEmitter<TreeView[]>();
  @Input() collectionId: string;
  @Output() emitCollectionId: EventEmitter<string>  = new EventEmitter<string>();              
  @Input() file: FileContent;

  ngOnInit() {
     this.carregarBredCrumb(this.collectionId);
  }

  ngOnChanges() {
    this.carregarBredCrumb(this.collectionId);
  }

  clickBredCrumb(collectionId: string){   
     /** spinner starts */
    this.spinner.show();
    this.carregarBredCrumb(collectionId);   
    this.emitCollectionId.emit(collectionId);
    setTimeout(() => {this.spinner.hide();},2000);

    return false;
  }


  carregarBredCrumb(collectionId: string){
    this.breadcrumbService.obterBreadCrumbPorCollectionId(collectionId)
    .then(response => {
      this.bredCrumbs = response;
      this.putItemList();
    });
  }

  /// coloca o item na lista para o breadcumb funcionar //
  putItemList(){
    if(this.file != null){
      this.bredCrumbs[this.bredCrumbs.length-1].nivel = "0";
      this.bredCrumbs.push({ id:'ss',name: this.file.name, nivel:"1"})
      this.file = null;
    }
  }

}
