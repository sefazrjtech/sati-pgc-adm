import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/app.api';
import {Pasta} from 'src/app/breadcrumb/pasta.model'
@Injectable()
export class BreadcrumbService {

  constructor(private http: HttpClient) { }

  obterBreadCrumbPorCollectionId(collectionId: string): Promise<Pasta[]>{
    console.log("breadCrumb: "+collectionId);
    return this.http.get<Pasta[]>(`${API}/pastas/breadcrumnb/${collectionId}`)
    .toPromise()
    .then(response => response)
    .catch(error => {
      console.log(error);
      return null;
    });
  }



}
